# Welcome to Material Markdown!
This is a simple and clean markdown editor with side by side preview.

![Screenshot](https://lh3.googleusercontent.com/bV6rFmBb9_-jiFztObWHyMg4Wijm8sgNqbIOMCqltWguhhsfnTUyvl2uYToe-1HxDYQFnEezFJQ=s1280-h800-e365-rw)

**Shortcuts**
- Load Sample Page: Ctrl+Shift+4
 - Mac: Cmd+4
- Open File: Ctrl+Shift+5
	- Mac: Cmd+5
- Save File: Ctrl+Shift+2
	- Mac: Cmd+2
- Save As File: Ctrl+Shift+3
	- Mac: Cmd+3
- Toggle Blockquote: Ctrl+'
- Toggle Bold: Ctrl+B
- Toggle Italic: Ctrl+I
- Draw Link: Ctrl+K
- Toggle Unordered List: Ctrl+L

**To Do's:**
- Search
- Autosaving
- Custom colors
- Recent files
- More customization

**Shortcuts**
- Load Sample Page: Ctrl+M
 - Mac: Cmd+4
- Open File: Ctrl+Q
	- Mac: Cmd+5
- Save File: Ctrl+Shift+1
	- Mac: Cmd+2
- Save As File: Ctrl+Shift+Y
	- Mac: Cmd+3
- Toggle Blockquote: Ctrl+'
- Toggle Bold: Ctrl+B
- Toggle Italic: Ctrl+I
- Draw Link: Ctrl+K
- Toggle Unordered List: Ctrl+L
- Toggle Blockquote: Ctrl+'
- Toggle Bold: Ctrl+B
- Toggle Italic: Ctrl+I
- Draw Link: Ctrl+K
- Toggle Unordered List: Ctrl+L

simplemde.options.previewRender("This is *example* Markdown");


> This app uses the open source SimpleMDE markdown editor